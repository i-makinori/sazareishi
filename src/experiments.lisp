
(in-package :sazareishi)

;; utils

(defmacro with-html (&body body)
  `(with-html-output-to-string (*standard-output* nil :prologue t)
     ,@body))


;; experiments

(defvar *acceptor* nil)

(defun start-easy-acceptor-server ()
  (stop-server)
  (hunchentoot:start
   (setf *acceptor*
         (make-instance 'hunchentoot:easy-acceptor :port 4042)))
  (asdf:oos 'asdf:load-op :hunchentoot-test))


(defun stop-server ()
  (when *acceptor*
    (hunchentoot:stop *acceptor*)))

#|
(hunchentoot:define-easy-handler (say-yo :uri "/") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "index です｡"))
|#


(hunchentoot:define-easy-handler (say-yo :uri "/sazare") (ishi)
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "サザレイシは､~@[ ~A~]です｡" ishi))




;;; subclass ACCEPTOR


(defparameter *sazare-directory*
  (merge-pathnames-as-directory *default-pathname-defaults*
                                "sazare/"))

(defun rekiishi-list (sazare-list)
  )

(defun open-file-as-text (file-path)
  )


(defun index ()
  (with-html
    (:html
     (:head (:title "hogehoge"))
     (:body
      (format nil "~A~%" *request*
              )
      "aaa"
      ))))







      

